from django.shortcuts import get_object_or_404
from rest_framework import generics, viewsets
from rest_framework.decorators import detail_route
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import SubjectSerializer, CourseSerializer, CourseWithContentSerializer
from ..models import Subject, Course


class SubjectListView(generics.ListAPIView):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class SubjectDetailView(generics.RetrieveAPIView):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class CourseViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    @detail_route(methods=['get'],
                  serializer_class=CourseWithContentSerializer)
    def contents(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

