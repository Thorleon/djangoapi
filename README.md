Fast example of Django REST, inspired by Antonio Mele *Django* book.
User after login can create courses, modules or different types content.

Credentials for superuser account:
login: thorleon
password: toor

Few API endpoints:

http://127.0.0.1:8000/api/courses/

http://127.0.0.1:8000/api/courses/1/

http://127.0.0.1:8000/api/courses/1/contents/

http://127.0.0.1:8000/api/subjects/